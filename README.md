# dilutBMS2

dilutBMS2 provides Bayesian Model Averaging for models with multicollinear regressors by including alternative priors such as different sets of Heredity priors introduced by [Chipman (1996)](http://onlinelibrary.wiley.com/doi/10.2307/3315687/full) and the Tessellation prior presented by [George (2010)](http://projecteuclid.org/euclid.imsc/1288099018). It is a fork of the [BMS](http://cran.r-project.org/web/packages/BMS/index.html) package for R written by Stefan Zeugner and Martin Feldkircher.

## Usage

For the Tessellation prior set

    :::r
    bms(..., mcmc="tess", pen=2)
    
 
the Strong Heredity prior is implemented in the default BMS package and can be accessed via

    :::r
    bms(..., mcmc="bd.int")

alternatively a Weak Heredity Prior can be specified, with a penalty term. The penalty puts a logarithmic penalty depending on the number of sampled Interaction terms and missing parent variables

    :::r
    bms(..., mprior="uniform.wh", mcmc="bd", pen=0.2)
    
the general Dilution Prior can be accessed through

    :::r
    bms(..., mprior="dilut")
